<?php
/**
  * Created by Guntars Vīksna
  * E-mail: Enimous@gmail.com
  * Date: 20.12.2019
  * Time: 10:24
*/
require_once __DIR__ . '/app/Autoload.php';

$app = new Application();
