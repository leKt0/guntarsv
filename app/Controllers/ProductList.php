<?php
/**
  * Created by Guntars Vīksna
  * E-mail: Enimous@gmail.com
  * Date: 20.12.2019
  * Time: 10:24
*/

class productList extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->view->productList = $this->model->productList();
        $this->view->render('productList/index');
    }



    public function edit($id)
    {
        $this->view->product = $this->model->productSingleList($id);
        $this->view->render('productList/edit');
    }

    public function editSave($id)
    {
        $data = array();
        $data['id'] = $id;
        $data['sku'] = $_POST['sku'];
        $data['name'] = $_POST['name'];
        $data['price'] = $_POST['price'];
        $data['type'] = $_POST['type'];
        $data['size'] = $_POST['size'];
        $data['weight'] = $_POST['weight'];
        $data['height'] = $_POST['height'];
        $data['width'] = $_POST['width'];
        $data['length'] = $_POST['length'];

        $this->model->editSave($data);
        header('Location: ' . URL . 'productList');
    }

    public function deleteButton($id)
    {
        $this->model->deleteButton($id);
        header('Location: ' .URL. 'productList');
    }

    public function delete()
    {
        $this->model->delete();
        header('Location: ' .URL. 'productList');
    }

    public function massDelete($sku)
    {
        $this->model->massDelete($sku);
        header('Location: ' .URL. 'productList');
    }
}
