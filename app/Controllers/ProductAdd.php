<?php
/**
  * Created by Guntars Vīksna
  * E-mail: Enimous@gmail.com
  * Date: 20.12.2019
  * Time: 10:24
*/

class productAdd extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->view->productAdd = $this->model->productAdd();
        $this->view->render('productAdd/index');
    }

    public function create()
    {
        $data = array();
        $data['sku'] = $_POST['sku'];
        $data['name'] = $_POST['name'];
        $data['price'] = $_POST['price'];
        $data['type'] = $_POST['type'];
        $data['size'] = $_POST['size'];
        $data['weight'] = $_POST['weight'];
        $data['height'] = $_POST['height'];
        $data['width'] = $_POST['width'];
        $data['length'] = $_POST['length'];

        // @TODO: Do your error checking!

        $this->model->create($data);
        header('Location: ' . URL . 'productAdd');
    }
}
