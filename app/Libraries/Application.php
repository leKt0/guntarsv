<?php
/**
  * Created by Guntars Vīksna
  * E-mail: Enimous@gmail.com
  * Date: 20.12.2019
  * Time: 10:24
*/

class Application
{
    public function __construct()
    {
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = explode('/', $url);

        if (empty($url[0])) {
            header('Location: productList');
            return false;
        }

        $file = 'app/controllers/' . $url[0] . '.php';
        if (file_exists($file)) {
            require_once $file;
        } else {
            header('Location: productAdd');
        }

        $controller = new $url[0];
        $controller->loadModel($url[0]);

        // calling methods
        if (isset($url[2])) {
            if (method_exists($controller, $url[1])) {
                $controller->{$url[1]}($url[2]);
            } else {
                $this->problem();
            }
        } else {
            if (isset($url[1])) {
                if (method_exists($controller, $url[1])) {
                    $controller->{$url[1]}();
                } else {
                    $this->problem();
                }
            } else {
                $controller->index();
            }
        }
        
    }

    public function problem()
    {
        require_once 'app/controllers/problem.php';
        $controller = new Problem();
        $controller->index();
        return false;
    }
}
