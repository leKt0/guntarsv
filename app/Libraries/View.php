<?php
/**
  * Created by Guntars Vīksna
  * E-mail: Enimous@gmail.com
  * Date: 20.12.2019
  * Time: 10:24
*/

class View
{
    public function __construct()
    {

    }

    public function render($name, $noInclude = false)
    {
        if ($noInclude == true) {
            require_once 'app/views/' . $name . '.php';
        } else {
            require_once 'app/views/_template/header.php';
            require_once 'app/views/' . $name . ".php";
            require_once 'app/views/_template/footer.php';
        }
    }
}
