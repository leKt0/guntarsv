<?php
/**
  * Created by Guntars Vīksna
  * E-mail: Enimous@gmail.com
  * Date: 20.12.2019
  * Time: 10:24
*/

class Controller
{
    public function __construct()
    {
        $this->view = new View();
    }

    public function loadModel($name)
    {
        $path = 'app/models/' . $name . '_model.php';

        if (file_exists($path)) {
            require_once 'app/models/' . $name . '_model.php';

            $modelName = $name . 'Model';
            $this->model = new $modelName();
        }
    }
}
