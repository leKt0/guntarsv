<?php
/**
  * Created by Guntars Vīksna
  * E-mail: Enimous@gmail.com
  * Date: 20.12.2019
  * Time: 10:24
*/
?>

<!DOCTYPE html>
<html class="theme--dark">

    <head>
        <meta charset="UTF-8">
        <?php 
        $pageTitle = substr($name, 0, strpos($name, '/'));
        ?>
        <title><?= $pageTitle; ?></title>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-1/css/all.css">
        <link rel="stylesheet" href="<?= URL; ?>public/css/app.css">
    </head>

    <body>
        <header>
            <nav class="navbar navbar-expand-lg">
                <a class="navbar-brand" href="<?= URL ?>">GuntarsV</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item <?= ($pageTitle == 'productList') ? 'active':''; ?>">
                            <a class="nav-link" href="<?= URL ?>productList">List Product</a>
                        </li>
                        <li class="nav-item <?= ($pageTitle == 'productAdd') ? 'active':''; ?>">
                            <a class="nav-link" href="<?= URL ?>productAdd">Add Product</a>
                        </li>
                        <li>
                            <label id="switch" class="switch">
                                <input type="checkbox" onchange="toggleTheme()" id="slider">
                                <span class="slider round"></span>
                            </label>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <main role="main">
