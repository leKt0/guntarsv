<?php
/**
  * Created by Guntars Vīksna
  * E-mail: Enimous@gmail.com
  * Date: 20.12.2019
  * Time: 10:24
*/
?>

</main>

<footer>
    <div class="container">
        <ul class="social-network social-circle">
            <li><a href="http://instagram.com/glostinthecloud" target="_blank"><img src="<?= URL; ?>public/img/instagram.png"></a></li>
            <li><a href="http://facebook.com/glostinthecloud" target="_blank"><img src="<?= URL; ?>public/img/facebook.png"></i></a></li>
            <li><a href="http://bitbucket.org/leKt0" target="_blank"><img src="<?= URL; ?>public/img/bitbucket.png"></i></a></li>
        </ul>
        <p>2020 &copy; Guntars Vīksna</p>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
<script src="https://getbootstrap.com/docs/4.4/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= URL; ?>public/js/main.js"></script>
<script type="text/javascript" src="<?= URL; ?>public/js/core.js"></script>
</body>

</html>
