<?php
/**
  * Created by Guntars Vīksna
  * E-mail: Enimous@gmail.com
  * Date: 20.12.2019
  * Time: 10:24
*/
?>
<div class="container">
    <form method="post" action="">
        <div class="row">
            <div class="col-12">
                <div class="input-group product-action">
                    <select class="custom-select" id="select_option">
                        <option disabled selected>Select Option</option>
                        <option value="mass_delete">Mass Delete</option>
                        <option value="delete_selected">Delete Selected</option>
                    </select>
                    <div class="input-group-append">
                        <span class="d-inline-block" tabindex="0" data-toggle="tooltip" data-original-title="Please select option to enable button!">
                            <input id="btn-submit" class="btn btn-danger" type="submit" name="submit" disabled>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container py-5">
            <div class="row">
                <?php foreach($this->productList as $key => $value): ?>
                <div class="col-sx-12 col-md-6 col-lg-3">
                    <div class="card mb-3">
                            <label class='checkbox danger'>
                                <input class="product-checkbox item" type="checkbox" name="checkbox[]" value="<?= $value['sku']?>" disabled>
                                <span class='indicator'></span>
                                Select Item
                            </label>
                        <img class=" card-img-top" alt="Card image cap">
                        <div class="card-body d-flex align-items-center justify-content-center">
                            <div class="row">
                                <div class="col-12 ml-4"><b>SKU:</b> <?= $value['sku'];?></div>
                                <div class="col-12 ml-4"><b>Name:</b> <?= $value['name'];?></div>
                                <div class="col-12 ml-4"><b>Price:</b> <?= $value['price'];?></div>
                                <?php if ($value['type'] == 'disc'): ?>
                                <div class="col-12 ml-4"><b>Size:</b> <?= $value['size'];?> MB</div>
                                <?php elseif ($value['type'] == 'book'): ?>
                                <div class="col-12 ml-4"><b>Weight:</b> <?= $value['weight'];?> KG</div>
                                <?php elseif ($value['type'] == 'furniture'): ?>
                                <div class="col-12 ml-4"><b>Dimension:</b> <?= $value['height'];?>x<?= $value['width'];?>x<?= $value['length'];?></div>
                                <?php endif; ?>
                                <div class="col-6 px-2 mt-3">
                                    <form></form> <!-- Bad Coding: Temp Fix, cuz u can't nest forms :'D -->
                                    <form action="productList/edit/<?= $value['id'] ?>" method="post">
                                        <button type="submit" class="btn btn-outline-success"><i class="fas fa-pen fa-lg"></i></button>
                                    </form>
                                </div>
                                <div class="col-6 px-5 mt-3">
                                <form action="productList/deleteButton/<?= $value['id'] ?>" method="post">
                                    <button type="submit" class="btn btn-outline-danger"><i class="fas fa-trash fa-lg"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </form>

    <div id="msg-1">
    </div>
    <div id="msg-2">
    </div>
</div>
