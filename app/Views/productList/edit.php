<?php
/**
  * Created by Guntars Vīksna
  * E-mail: Enimous@gmail.com
  * Date: 20.12.2019
  * Time: 10:24
*/
?>

<div class="container product-add-form">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <form id="myForm" action="<?= URL ?>productList/editSave/<?= $this->product['id'];?>" method="post">
                <div class="form-group row required">
                    <label for="sku" class="col-sm-3 col-form-label">SKU</label>
                    <input class="form-control col-sm-9" type="text" name="sku" id="sku" value="<?= $this->product['sku']; ?>" required>
                </div>

                <div class="form-group row required">
                    <label for="name" class="col-sm-3 col-form-label">Name</label>
                    <input type="text" class="form-control col-sm-9" name="name" id="name" value="<?= $this->product['name']; ?>" required>
                </div>

                <div class="form-group row required">
                    <label for="price" class="col-sm-3 col-form-label">Price</label>
                    <input type="number" step=".01" class="form-control col-sm-9" name="price" id="price" value="<?= $this->product['price']; ?>" required>
                </div>

                <div class="form-group row required">
                    <label for="select_type" class="col-sm-3 col-form-label">Type</label>
                    <select class="form-control form-control col-sm-9" id="select_type" name="type" required>
                        <option selected disabled>Select product type</option>
                        <option value="disc" <?php if ($this->product['type'] == 'disc'): echo 'selected'; endif; ?>>Disc</option>
                        <option value="book" <?php if ($this->product['type'] == 'book'): echo 'selected'; endif; ?>>Book</option>
                        <option value="furniture" <?php if ($this->product['type'] == 'furniture'): echo 'selected'; endif; ?>>Furniture
                    </select>
                </div>
                <div class="second-form">
                    <div id="disc" <?php if ($this->product['type'] == 'book' || $this->product['type'] == 'furniture'): echo 'style="display: none"'; endif; ?>>
                        <div class="form-group row required">
                            <label for="size" class="col-sm-3 col-form-label">Size</label>
                            <input class="form-control col-sm-9" type="number" step=".01" name="size" value="<?php echo $this->product['size'] ?>"><br>
                        </div>
                        <div class="form-group">
                            <p class="alert alert-warning">
                                <span>Please provide disc size in MB, when type <b>Disc</b> is selected!</span>
                            </p>
                        </div>
                    </div>

                    <div id="book" <?php if ($this->product['type'] == 'disc' || $this->product['type'] == 'furniture'): echo 'style="display: none"'; endif; ?>>
                        <div class="form-group row required">
                            <label for="weight" class="col-sm-3 col-form-label">Weight</label>
                            <input class="form-control col-sm-9" type="number" step=".01" name="weight" value="<?php echo $this->product['weight'] ?>"><br>
                        </div>
                        <div class="form-group">
                            <p class="alert alert-warning">
                                <span>Please provide book weight in KG, when type <b>Book</b> is selected!</span>
                            </p>
                        </div>
                    </div>

                    <div id="furniture" <?php if ($this->product['type'] == 'disc' || $this->product['type'] == 'book'): echo 'style="display: none"'; endif; ?>>
                        <div class="form-group row required">
                            <label for="height" class="col-sm-3 col-form-label">Height</label>
                            <input class="form-control col-sm-9" type="number" name="height" value="<?php echo $this->product['height'] ?>"><br>
                        </div>
                        <div class="form-group row required">
                            <label for="width" class="col-sm-3 col-form-label">Width</label>
                            <input class="form-control col-sm-9" type="number" name="width" value="<?php echo $this->product['width'] ?>"><br>
                        </div>
                        <div class="form-group row required">
                            <label for="length" class="col-sm-3 col-form-label">Length</label>
                            <input class="form-control col-sm-9" type="number" name="length" value="<?php echo $this->product['length'] ?>"><br>
                        </div>
                        <div class="form-group">
                            <p class="alert alert-warning">
                                <span>Please provide dimensions in HxWxL format, when type <b>Furniture</b> is selected!</span>
                            </p>
                        </div>
                    </div>
                </div>
                <button id="submit" class="btn btn-success" type="submit">Submit</button>
            </form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
