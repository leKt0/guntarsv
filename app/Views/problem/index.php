<?php
/**
  * Created by Guntars Vīksna
  * E-mail: Enimous@gmail.com
  * Date: 20.12.2019
  * Time: 10:24
*/
?>

<div class="container py-5">
  <div class="row justify-content-center">
    <div class="col-6 text-center">
      <div class="alert alert-danger alert-text">
        <?= $this->msg?>
      </div>
    </div>
  </div>
</div>