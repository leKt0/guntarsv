<?php
/**
  * Created by Guntars Vīksna
  * E-mail: Enimous@gmail.com
  * Date: 20.12.2019
  * Time: 10:24
*/
?>

<div class="container product-add-form">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <form id="add_form" action="<?= URL ?>productAdd/create" method="post">
                <div class="form-group row required">
                    <label for="sku" class="col-sm-3 col-form-label">SKU</label>
                    <input class="form-control col-sm-9" type="text" name="sku" id="sku" placeholder="Product SKU (Unique)" required>
                </div>

                <div class="form-group row required">
                    <label for="name" class="col-sm-3 col-form-label">Name</label>
                    <input type="text" class="form-control col-sm-9" name="name" id="name" placeholder="Product name" required>
                </div>

                <div class="form-group row required">
                    <label for="price" class="col-sm-3 col-form-label">Price</label>
                    <input type="number" step=".01" class="form-control col-sm-9" name="price" id="price" placeholder="Product price" required>
                </div>

                <div class="form-group row required">
                    <label for="select_type" class="col-sm-3 col-form-label">Type</label>
                    <select class="form-control form-control col-sm-9" id="select_type" name="type" required>
                        <option selected disabled>Select product type</option>
                        <option value="disc">Disc</option>
                        <option value="book">Book</option>
                        <option value="furniture">Furniture</option>
                    </select>
                </div>
                <div class="second-form">
                    <div class="type_block" id="disc">
                        <div class="form-group row required">
                            <label for="size" class="col-sm-3 col-form-label">Size</label>
                            <input class="form-control col-sm-9" type="number" step=".01" name="size">
                        </div>
                        <div class="form-group">
                            <p class="alert alert-warning">
                                <span>Please provide disc size in MB, when type <b>Disc</b> is selected!</span>
                            </p>
                        </div>
                    </div>

                    <div class="type_block" id="book">
                        <div class="form-group row required">
                            <label for="weight" class="col-sm-3 col-form-label">Weight</label>
                            <input class="form-control col-sm-9" type="number" step=".01" name="weight">
                        </div>
                        <div class="form-group">
                            <p class="alert alert-warning">
                                <span>Please provide book weight in KG, when type <b>Book</b> is selected!</span>
                            </p>
                        </div>
                    </div>

                    <div class="type_block" id="furniture">
                        <div class="form-group row required">
                            <label for="height" class="col-sm-3 col-form-label">Height</label>
                            <input class="form-control col-sm-9" type="number" name="height">
                        </div>
                        <div class="form-group row required">
                            <label for="width" class="col-sm-3 col-form-label">Width</label>
                            <input class="form-control col-sm-9" type="number" name="width">
                        </div>
                        <div class="form-group row required">
                            <label for="length" class="col-sm-3 col-form-label">Length</label>
                            <input class="form-control col-sm-9" type="number" name="length">
                        </div>
                        <div class="form-group">
                            <p class="alert alert-warning">
                                <span>Please provide dimensions in HxWxL format, when type <b>Furniture</b> is selected!</span>
                            </p>
                        </div>
                    </div>
                </div>
                <button id="btn-submit" class="btn btn-danger" type="submit" onclick="successMessage();" disabled>Submit</button>
                </div>

            </form>
    </div>
</div>
</div>
