<?php
/**
  * Created by Guntars Vīksna
  * E-mail: Enimous@gmail.com
  * Date: 20.12.2019
  * Time: 10:24
*/
?>

<div class="container text-center">
    <h2>Checkboxes</h2>
    <div class="row justify-content-center">
        <div class="col-12 d-flex justify-content-center">
            <label class='checkbox'>
                <input type='checkbox'>
                <span class='indicator'></span>
                Unchecked
            </label>
            <label class='checkbox'>
                <input type='checkbox' checked disabled>
                <span class='indicator'></span>
                Disabled
            </label>
        </div>
        <h5>Checked </h5>
        <div class="col-12 d-flex justify-content-center">
            <label class='checkbox dark'>
                <input type='checkbox' checked>
                <span class='indicator'></span>
                Dark
            </label>
            <label class='checkbox light'>
                <input type='checkbox' checked>
                <span class='indicator'></span>
                Light
            </label>

            <label class='checkbox success'>
                <input type='checkbox' checked>
                <span class='indicator'></span>
                Success
            </label>

            <label class='checkbox warning'>
                <input type='checkbox' checked>
                <span class='indicator'></span>
                Warning
            </label>

            <label class='checkbox danger'>
                <input type='checkbox' checked>
                <span class='indicator'></span>
                Danger
            </label>

            <label class='checkbox femine'>
                <input type='checkbox' checked>
                <span class='indicator'></span>
                Femine
            </label>
        </div>
    </div>
    <h2>Buttons</h2>
    <div class="row justify-content-center">
        <h5>Button types</h5>
        <div class="col-12 d-flex justify-content-center">
            <button type="submit" class="btn btn-dark">Dark</button>
            <button type="submit" class="btn btn-light">Light</button>
            <button type="submit" class="btn btn-success">Success</button>
            <button type="submit" class="btn btn-warning">Warning</button>
            <button type="submit" class="btn btn-danger">Danger</button>
            <button type="submit" class="btn btn-femine">Femine</button>
        </div>
        <h5>Disabled</h5>
        <div class="col-12 d-flex justify-content-center">
            <button type="submit" class="btn btn-dark" disabled>Dark</button>
            <button type="submit" class="btn btn-light" disabled>Light</button>
            <button type="submit" class="btn btn-success" disabled>Success</button>
            <button type="submit" class="btn btn-warning" disabled>Warning</button>
            <button type="submit" class="btn btn-danger" disabled>Danger</button>
            <button type="submit" class="btn btn-femine" disabled>Femine</button>
        </div>
        <h5>Outline</h5>
        <div class="col-12 d-flex justify-content-center">
            <button type="submit" class="btn btn-outline-dark">Dark</button>
            <button type="submit" class="btn btn-outline-light">Light</button>
            <button type="submit" class="btn btn-outline-success">Success</button>
            <button type="submit" class="btn btn-outline-warning">Warning</button>
            <button type="submit" class="btn btn-outline-danger">Danger</button>
            <button type="submit" class="btn btn-outline-femine">Femine</button>
        </div>
        <h5>Outline Disabled</h5>
        <div class="col-12 d-flex justify-content-center">
            <button type="submit" class="btn btn-outline-dark" disabled>Dark</button>
            <button type="submit" class="btn btn-outline-light" disabled>Light</button>
            <button type="submit" class="btn btn-outline-success" disabled>Success</button>
            <button type="submit" class="btn btn-outline-warning" disabled>Warning</button>
            <button type="submit" class="btn btn-outline-danger" disabled>Danger</button>
            <button type="submit" class="btn btn-outline-femine" disabled>Femine</button>
        </div>
    </div>
</div>
