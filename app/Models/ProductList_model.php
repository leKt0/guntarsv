<?php
/**
  * Created by Guntars Vīksna
  * E-mail: Enimous@gmail.com
  * Date: 12.06.2018
*/

class ProductListModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Selecting all Products from database
     */

    public function productList()
    {
        //$sth = $this->db->prepare('SELECT id, sku, name, price, type, size, weight, height, width, length FROM products');
        $sth = $this->db->prepare("SELECT * FROM products");
        $sth->execute();
        return $sth->fetchAll();
    }

    /**
     * Selecting single product from database
     */

    public function productSingleList($id)
    {
        $sth = $this->db->prepare('SELECT id, sku, name, price, type, size, weight, height, width, length FROM products WHERE id = :id');
        $sth->execute(array(':id' => $id));
        return $sth->fetch();
    }

    /**
     * Editing products & writing to database
     */

    public function editSave($data)
    {
        $sth = $this->db->prepare('UPDATE products SET `sku` = :sku, `name` = :name,
            `price` = :price, `type` = :type, `size` = :size, `weight` = :weight,
            `height` = :height, `width` = :width, `length` = :length WHERE id = :id');

        $sth->execute(array(
            ':id' => $data['id'],
            ':sku' => $data['sku'],
            ':name' => $data['name'],
            ':price' => $data['price'],
            ':type' => $data['type'],
            ':size' => $data['size'],
            ':weight' => $data['weight'],
            ':height' => $data['height'],
            ':width' => $data['width'],
            ':length' => $data['length']
        ));
    }

    /**
     * Deleting products using button
     */

    public function deleteButton($id)
    {
        $sql = "DELETE FROM products WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);

        $query->execute($parameters);
    }

    /**
     * Deleting products from database
     */

    public function delete()
    {
        if(isset($_POST['submit'])) {
            $checkbox = $_POST['checkbox'];
            for($i = 0; $i < count($checkbox); $i++) {
                $del_id = $checkbox[$i];
                print($del_id);
                $sth = $this->db->prepare("DELETE FROM products WHERE sku = '" . $del_id . "'");
                $sth->execute(array(
                    ':sku' => $del_id
                ));
            }
        }
    }

    /**
     * Mass deleting products from database
     */

    public function massDelete($sku)
    {
        $sth = $this->db->prepare('DELETE FROM products WHERE sku = :sku');
        $sth->execute(array(
            ':sku' => $sku
        ));
    }
}
