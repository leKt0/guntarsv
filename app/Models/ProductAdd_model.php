<?php
/**
 * Created by Guntars Vīksna
 * E-mail: Enimous@gmail.com
 * Date: 15.01.2018
 */

class ProductAddModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Selecting all Products from database
     */

    public function productAdd()
    {
        //$sth = $this->db->prepare('SELECT id, sku, name, price, type, size, weight, height, width, length FROM products');
        $sth = $this->db->prepare("SELECT * FROM products");
        $sth->execute();
        return $sth->fetchAll();
    }

    /**
     * Creating new products & writing to database
     */

    public function create($data)
    {
        $sth = $this->db->prepare('INSERT INTO products
                (`sku`, `name`, `price`, `type`, `size`, `weight`, `height`, `width`, `length`)
                VALUES (:sku, :name, :price, :type, :size, :weight, :height, :width, :length)
                ');

        $sth->execute(array(
            ':sku' => $data['sku'],
            ':name' => $data['name'],
            ':price' => $data['price'],
            ':type' => $data['type'],
            ':size' => $data['size'],
            ':weight' => $data['weight'],
            ':height' => $data['height'],
            ':width' => $data['width'],
            ':length' => $data['length']
        ));
    }

    /**
     * Deleting products from database
     */

    public function delete($id)
    {
        $sth = $this->db->prepare('DELETE FROM products WHERE id = :id');
        $sth->execute(array(
            ':id' => $id
        ));
    }
}
