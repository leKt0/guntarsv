<?php
/**
  * Created by Guntars Vīksna
  * E-mail: Enimous@gmail.com
  * Date: 20.12.2019
  * Time: 10:24
*/

require_once __DIR__ . '/Config/Database.php';
require_once __DIR__ . '/Config/Paths.php';

spl_autoload_register(function ($className) {

	$className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
	include_once __DIR__ . '/Libraries/' . $className . '.php';

});
spl_autoload_register(function ($className) {

	$className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
	include_once __DIR__ . '/Controllers/' . $className . '.php';

});
spl_autoload_register(function ($className) {

	$className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
	include_once __DIR__ . '/Models/' . $className . '.php';

});
/*
function autoloadConfigs($className) {
    $filename = "app/config/" . $className . ".php";
    if (is_readable($filename)) {
        require $filename;
    }
}
*/
/*
function autoloadLibraries($className) {
    $filename = "app/Libraries/" . $className . ".php";
    if (is_readable($filename)) {
        require $filename;
    }
}
*/
//spl_autoload_register("autoloadConfigs");
//spl_autoload_register("autoLoadLibraries");
