class Core
{
    constructor()
    {
        self = this;
    }

    show_alert(message, type = 'success')
    {
        $('body').append("<div id='msg_box' class='" + type + "'>" + message + "</div>");
        setTimeout(function () {
            $("#msg_box").remove();
        }, 2000);
    }

    addProduct(sku, name, price, type, size, weight, height, width, length) {
        self.show_alert("Saving products...");
        $.ajax({
            type: 'POST',
            url: "?controller=AjaxController&action=create  ",
            data: {
                'sku': sku,
                'name': name,
                'price': price,
                'type': type,
                'size': size,
                'weight': weight,
                'height': height,
                'width': width,
                'length': length
            },
            success: function (msg) {
                if(msg === "OK"){
                    self.show_alert("Product added successfully!");
                    $("#add_form").trigger('reset');
                } else {
                    self.show_alert("Something went wrong...", "error");
                }
            },
            error: function () {
                console.log("yeet");
                self.show_alert("Something went wrong...", "error");
            },
            async: false
        });
    }
}