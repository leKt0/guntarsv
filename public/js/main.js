/*jslint browser: true*/
/*global $, document*/

$(document).ready(function () {

    $('#slider').change(function () {
        if (this.checked) {
            $('.card-img-top').attr('src', 'https://dummyimage.com/200x205/aaa6a3/131110&text=Thumbnail');
        } else {
            $('.card-img-top').attr('src', 'https://dummyimage.com/200x205/333/eceeef&text=Thumbnail');
        }
    });

    $("#select_type").on("change", function () {
        var type = $("#select_type").val();
        var panel = $("#" + type);
        var inputs = panel.find("input");

        $('.second-form').find("input").filter('[required]').prop('required', false);
        $(inputs).prop('required', true);

        $('.second-form').find("div").filter('[style]').css({
            display: 'none'
        });
        $(panel).css({
            display: 'block'
        });
        $(panel).prop('required', true);

        $("#btn-submit").prop('disabled', false).css({
            cursor: 'pointer',
            pointerEvents: ''
        }).removeClass('btn-danger').addClass('btn-success');
        $(".d-inline-block").removeAttr('data-toggle data-original-title');
    });

    $("#select_option").on("change", function () {
        $("#btn-submit").prop('disabled', false).css({
            cursor: 'pointer',
            pointerEvents: ''
        }).removeClass('btn-danger').addClass('btn-success');
        $(".d-inline-block").removeAttr('data-toggle data-original-title');
    });

    $('#select_option').change(function () {
        if ($(this).val() == 'mass_delete') {
            $("input:checkbox.item").prop('disabled', false);

            $("input:checkbox.item").click(function () {
                $("input:checkbox.item").not(this).prop('checked', false);

                if ($(this).is(':checked')) {
                    $("input:submit").prop('disabled', false);
                    $("form").attr('action', "productList/massDelete/" + $(this).prop('value'));
                } else {
                    $("input:submit").prop('disabled', true);
                }
            });
        }
        if ($(this).val() == 'delete_selected') {
            $("input:checkbox.item").prop('disabled', false);

            $("input:checkbox.item").click(function () {
                if ($("input:checkbox.item").is(':checked')) {
                    $("input:submit").prop('disabled', false);
                    $("form").attr('action', "productList/delete/");
                } else {
                    $("input:submit").prop('disabled', true);
                }
            });
        }
    });
});

// function to set a given theme/color-scheme
function setTheme(themeName) {
    localStorage.setItem('theme', themeName);
    document.documentElement.className = themeName;
}
// function to toggle between light and dark theme
function toggleTheme() {
    if (localStorage.getItem('theme') === 'theme--dark') {
        setTheme('theme--light');
        $('#slider').prop('checked', true);
        $('.card-img-top').attr('src', 'https://dummyimage.com/200x205/aaa6a3/131110&text=Thumbnail');
    } else {
        setTheme('theme--dark');
        $('#slider').prop('checked', false);
        $('.card-img-top').attr('src', 'https://dummyimage.com/200x205/333/eceeef&text=Thumbnail');
    }
}
// Immediately invoked function to set the theme on initial load
(function () {
    if (localStorage.getItem('theme') === 'theme--dark') {
        setTheme('theme--dark');
        $('#slider').prop('checked', false);
        $('.card-img-top').attr('src', 'https://dummyimage.com/200x205/333/eceeef&text=Thumbnail');
    } else {
        setTheme('theme--light');
        $('#slider').prop('checked', true);
        $('.card-img-top').attr('src', 'https://dummyimage.com/200x205/aaa6a3/131110&text=Thumbnail');
    }
})();
