## Changelog

#### 18/12/2019
* Removed Bootstrap, pulled needed components from Bootstrap to my own framework for an idea & future redesign.
* Completed Dark/Light theme: Navbar, Toggler Icon, Footer + icons.
* Made some changes in Product Card layout.
* Fixed spacing issue on Add/Edit product forms.
* Added a dumb form fix for nested forms to make first product editable/deletable.
* Added dynamic active class for Nav-Links, current page = active link on navbar.
* Fixed Database structure for Disc Size, now you can actually write size in megabytes...
* Looked into ***namespace*** & ***autoloader*** usage.
* Read about Data Access Layer (DAL).

#### 17/12/2019
* Seperated CSS & SCSS folders for better workflow.
* Made some changes in base, framework partials (SCSS).
* Completed Dark Theme and Light Theme (theme-switcher, themes partials).
* Added a Theme Switch toggler next to Page navigation.
* Fixed ***Product List*** item displaying using a wrapping Flexbox.
* Fixed ***Product Description*** by using a flexbox layout inside the card body.
* Fixed ***Product Add*** page form spacing.
* Re-designed the ***Problem/Error*** page.
* Small bug fixes in header & footer.

#### 16/12/2019
* Added a completely new CSS base for the whole project. (Still need to, create some stuff to completely remove Bootstrap).
* Fixed Product List & Product Add views (Tidy HTML, still needs improvements).
* Worked on controllers & models, added a delete function for Product List Item, dedicated delete buttons.
* Removed unnecessary functions from Product Add controller & model.
* Added Checkboxes on Product List Item cards.
* Wrote jQuery to add functionality & restrictions for item checkboxes.
* Added user restrictions. Example: Form buttons don't work, if a required field is not filled or selected.
* Started working on the insane 2019/2020 trend - Dark Theme, may the precious eyes of programmers rest 😂
* Other small bug fixes & improvements...

---

## TO-DO List

#### 18/12/2019
* Start using SCSS Extend/Inheritance to make the code even cleaner.
* ~~Create new components for my own CSS framework so I can remove Bootstrap & improve page load time.~~
* Go trough PSR-1 - PSR-4
* Look into using Namespaces

#### 17/12/2019
* ~~Rewrite whole CSS~~
* Create alerts after actions
* Fix hard-coded views
* Make the problem controller/view actually useful.
* ~~Make use of dev mode.~~
* Start working on your own UNIQUE design (Add smooth animations for forms, etc.)
* ~~Cleanup CSS overall, get rid of useless stuff.~~

---

## Scandiweb Junior test

Simple MVC where you can add/delete products.

---

## Database Configuration

---
* Database connection can be configured under app/config/database.php
* Database SQL file included in root directory of project - "guntarsv.sql"

## Project Path Configuration

---
* Project path can be configured under app/config/paths.php