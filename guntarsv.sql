-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2019 at 02:05 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `guntarsv`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `sku` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `type` varchar(20) NOT NULL,
  `size` decimal(3,1) DEFAULT NULL,
  `weight` varchar(20) DEFAULT NULL,
  `height` int(20) DEFAULT NULL,
  `width` int(20) DEFAULT NULL,
  `length` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku`, `name`, `price`, `type`, `size`, `weight`, `height`, `width`, `length`) VALUES
(14, 'GGWP007', 'War & Peace', '4.99', 'book', '0.0', '1.25', 0, 0, 0),
(15, 'GGWP007', 'War & Peace', '4.99', 'book', '0.0', '1.25', 0, 0, 0),
(16, 'GGWP008', 'War & Peace', '4.99', 'book', '0.0', '1.25', 0, 0, 0),
(17, 'GGWP009', 'War & Peace', '4.99', 'book', '0.0', '1.25', 0, 0, 0),
(18, 'GGWP10', 'War & Peace', '4.99', 'book', '0.0', '1.25', 0, 0, 0),
(19, 'TRJK01', 'ACME Disc', '1.99', 'disc', '99.9', '', 0, 0, 0),
(20, 'TRJK02', 'ACME Disc', '1.99', 'disc', '99.9', '', 0, 0, 0),
(21, 'TRJK03', 'ACME Disc', '1.99', 'disc', '99.9', '', 0, 0, 0),
(22, 'AGMN001', 'IKEA Table', '24.99', 'furniture', '0.0', '', 40, 80, 160),
(23, 'AGMN002', 'IKEA Table', '24.99', 'furniture', '0.0', '', 40, 80, 160);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
